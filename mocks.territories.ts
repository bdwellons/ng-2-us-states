import { State } from './state';

export const TERRITORIES: State[] = [{
            "name": "Virgin Islands",
            "abbreviation": "VI"
        }, {
            "name": "Puerto Rico",
            "abbreviation": "PR"
        }, {
            "name": "Palau",
            "abbreviation": "PW"
        }, {
            "name": "Northern Mariana Islands",
            "abbreviation": "MP"
        }, {
            "name": "Marshall Islands",
            "abbreviation": "MH"
        }, {
            "name": "Guam",
            "abbreviation": "GU"
        }, {
            "name": "American Samoa",
            "abbreviation": "AS"
        }, {
            "name": "District Of Columbia",
            "abbreviation": "DC"
        }, {
            "name": "Federated States Of Micronesia",
            "abbreviation": "FM"
        }];